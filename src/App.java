import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) throws Exception {

        long startTime = System.currentTimeMillis();

        String[] urls = { "https://en.wikipedia.org/wiki/Main_Page",
                "https://www.youtube.com/",
                "https://www.bbc.com/news",
                "https://www.google.com/",
                "https://www.facebook.com/",
                "https://www.twitter.com/",
                "https://www.instagram.com/",
                "https://www.reddit.com/",
                "https://www.linkedin.com/",
                "https://www.amazon.com/",
                "https://www.netflix.com/",
                "https://www.microsoft.com/",
                "https://www.apple.com/",
                "https://www.spotify.com/",
                "https://www.airbnb.com/",
                "https://www.ebay.com/",
                "https://www.edition.cnn.com/",
                "https://www.nytimes.com/",
                "https://www.nationalgeographic.com/",
                "https://www.nasa.gov/",
                "https://www.ted.com/",
                "https://www.imdb.com/",
                "https://www.github.com/",
                "https://www.stackoverflow.com/",
                "https://www.adobe.com/",
                "https://www.pinterest.com/",
                "https://www.tumblr.com/",
                "https://www.medium.com/",
                "https://www.quora.com/",
                "https://www.dev.to/",
                "https://www.yelp.com/",
                "https://www.tripadvisor.com/",
                "https://www.dropbox.com/",
                "https://www.slack.com/",
                "https://www.zoom.us/",
                "https://www.whatsapp.com/",
                "https://www.uber.com/",
                "https://www.airbnb.com/",
                "https://www.pinterest.com/",
                "https://www.twitch.tv/",
                "https://www.hulu.com/",
                "https://www.alibaba.com/",
                "https://www.shopify.com/",
                "https://www.coursera.org/",
                "https://www.udemy.com/",
                "https://www.khanacademy.org/",
                "https://www.duolingo.com/",
                "https://www.wordpress.com/",
                "https://www.blogger.com/",
                "https://www.tumblr.com/",
                "https://www.booking.com/",
                "https://www.hotels.com/",
                "https://www.expedia.com/",
                "https://www.agoda.com/",
                "https://www.trivago.com/",
                "https://www.marriott.com/",
                "https://www.hilton.com/",
                "https://www.accorhotels.com/",
                "https://www.ihg.com/",
                "https://www.choicehotels.com/",
                "https://www.upb.edu.co/"
        };

        // threadpool to execute jobs concurrently
        int maxConcurrentJobs = 3;
        ExecutorService executor = Executors.newFixedThreadPool(maxConcurrentJobs); // queue jobs that exceed maxcjobs

        List<Converter> converters = new ArrayList<>();

        // submit sends a runnable job to execute it
        // job is schedule in one of threads of pool
        for (String url : urls) {
            Converter converter = new Converter(url);
            converters.add(converter);
            executor.submit(converter);
        }

        executor.shutdown();

        // blocks main program, until previous jobs be done
        try {
            executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("All conversions completed");

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println("Total time: " + totalTime / 1000 + " seconds");

        for (Converter converter : converters) {
            System.out.println("URL: " + converter.getUrl() + "PDF location: " + converter.getOutputPath());
        }
    }
}
