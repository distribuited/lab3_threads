public class Document {
    private String name;
    private String location;

    public Document(String name, String location) {
        this.name = name;
        this.location = location;
    }

    @Override
    public String toString() {
        return "Document [name=" + name + ", location=" + location + "]";
    }
}
