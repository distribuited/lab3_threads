import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Converter implements Runnable {

    private static int threadNumber = 0;
    private String url;
    private Lock lock = new ReentrantLock();
    private String outputPath;

    public Converter(String url) {
        this.url = url;
    }

    // override interface method run
    @Override
    public void run() {

        String localDir = System.getProperty("user.dir");

        String threadName = Thread.currentThread().getName();
        int currentThreadNumber;

        // avoid concurrence conflicts with the index with lock
        lock.lock();
        try {
            currentThreadNumber = threadNumber++;
            outputPath = localDir + "/pdf/output-" + currentThreadNumber + ".pdf";
        } finally {
            lock.unlock();
        }

        if (url != null) {
            String[] command = {
                    "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
                    "--headless",
                    "--disable-gpu",
                    "--print-to-pdf=" + outputPath,
                    url
            };

            // generator process
            ProcessBuilder processBuilder = new ProcessBuilder(command);

            try {
                Process process = processBuilder.start();
                int exitCode = process.waitFor();
                if (exitCode == 0) {
                    System.out.println("PDF generated from: " + url + " from thread: " + threadName);
                } else {
                    System.err.println("PDF generation failed for: " + url);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public String getUrl() {
        return url;
    }

    public String getOutputPath() {
        return outputPath;
    }
}
